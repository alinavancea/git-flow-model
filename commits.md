
#### [< Best practices](best-practices.md)   -   [Branches >](branches.md)
---

# Commits

_Commit messages show whether a developer is a good collaborator_

* Commit messages should contain an explanation of the changes, short answer of why we do the changes
* Commit messages should have consistent formatting
* Start with a capital letter
* Leave new line between paragraphs
* Use bullet points asterix * or -
* Don’t end with a . we want to keep the message as short as possible
* Keep it short and simple
* Commits should be one change, do not mix changes or tickets
* Commit messages might include metadata to reference project and tickets ID
* Commit messages should be imperative, start with “Add”, “Fix”, “Change” not “Fixing”, “Change”, “Fixed”
* Commit messages as “Bug”, “Fix”, “Change” are not a good practice

Examples: 

```
git commit -m "GFM-#15: Add users path to Admin module"
```

Opens `vi` where you can add multiline commit message

```
git commit
```

```
Short (50 chars or less) summary of changes

More detailed explanatory text, if necessary.  Wrap it to about 72
characters or so.  In some contexts, the first line is treated as the
subject of an email and the rest of the text as the body.  The blank
line separating the summary from the body is critical (unless you omit
the body entirely); tools like rebase can get confused if you run the
two together

Further paragraphs come after blank lines

  - Bullet points are okay, too

  - Typically a hyphen or asterisk is used for the bullet, preceded by a
    single space, with blank lines in between, but conventions vary here

Resolves GFM-#14. See GFM-#12 and GFM-#13
```

---
#### [< Best practices](best-practices.md)   -   [Branches >](branches.md)
---
