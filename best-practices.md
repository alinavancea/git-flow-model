#### [< Basic Git commands](basic-git-commands.md)   -   [Commits >](commits.md)
---

# Best practices

* Commit early and often
* Use single purpose commits
* Write meaningful commit messages
* Be specific with git commands and specific the arguments
* Use feature branches even for one person project, don't commit direct to master
* Always keep the `master` branch clean
* Squash commits when it make sense
* Keep your branch up to date
* Don't commit generated files, build a meaningful .gitignore
* Don't commit passwords, api keys etc

---
#### [< Basic Git commands](basic-git-commands.md)   -   [Commits >](commits.md)
---
