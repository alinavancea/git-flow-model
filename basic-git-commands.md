
#### [< What is Git?](what-is-git.md)   -   [Best practices >](best-practices.md)
---

# Basic Git commands

Each command has a link to the official git manual docs where you can find detailed descriptions of how commands are working including all possible options.

## [Clone](https://git-scm.com/docs/git-clone)

Get the copy of an existing project

```
git clone <url>
```

Example

```
git clone git@gitlab.com:alinavancea/git-flow-model.git
```

## [Add changes to index](https://git-scm.com/docs/git-add)

Add all files

```
git add .
```

Add specific file

```
git add <file path>
```

## [Commit a change](https://git-scm.com/docs/git-commit)

### With a short message

```
git commit -m <message>
```

```
git commit -m "GFM-3: Add Basic commands used in Git"

```

### With a longer message

Opens the `vi` and allows you to write a multiline message

```
git commit
```

```
Short (50 chars or less) summary of changes

More detailed explanatory text, if necessary.  Wrap it to about 72
characters or so.  In some contexts, the first line is treated as the
subject of an email and the rest of the text as the body.  The blank
line separating the summary from the body is critical (unless you omit
the body entirely); tools like rebase can get confused if you run the
two together
 
Further paragraphs come after blank lines
 
   - Bullet points are okay, too
 
   - Typically a hyphen or asterisk is used for the bullet, preceded by a
     single space, with blank lines in between, but conventions vary here

Resolves GFM-3. See GFM-12 and GFM-13
```

## [Push changes to remote](https://git-scm.com/docs/git-push)

Updates remote master branch with the changes on the current branch

```
git push origin master
```

## [Update local with the remote changes](https://git-scm.com/docs/git-pull)

Updates local branch with the changes from remote master branch

```
git pull origin master
```

## [Move between branches](https://git-scm.com/docs/git-checkout)

```
git checkout <branch name>
```

## [See the commit logs](https://git-scm.com/docs/git-log)

``` 
git log
```
> commit d02c73caff8c329f207298e5b20c568f5035a775 (HEAD -> master, origin/master, origin/HEAD, origin/GFM-3.alina.add-basic-and-most-used-commands-in-git)
> Author: Alina <alina.vancea@gmail.com>
> Date:   Fri Jan 10 09:44:46 2020 +0000
> 
>    GFM-3: Add initial contents page
> 
> commit 47460244cd8943af8e1dba913eabe418000196e2
> Author: Alina <alina.vancea@gmail.com>
> Date:   Fri Jan 10 09:40:35 2020 +0000
> 
>     GFM-1: Add README.md

## [Show a specific commit](https://git-scm.com/docs/git-show)

```
git show <commit>
```

## [Merge changes](https://git-scm.com/docs/git-merge)

Merge the changes from master in current branch 
```
git merge master
```

Create a merge commit in all cases, even when the merge could instead be resolved as a fast-forward.

```
git merge master --no-ff
```


## [Rebase commits](https://git-scm.com/docs/git-rebase)

Reapply commits on top of another base tip

```
git rebase -i <branch>
```

Rebase commits on current branch giving the chance to squash commits together. Opens `vi` where you have the possibility to review commits.
```
git rebase -i HEAD~2 
```

Rebase offers multiple options including changing older commits meessages

```
pick e499d89 Delete CNAME
pick 0c39034 Better README
pick f7fde4a Change the commit message but push the same commit.

# Rebase 9fdb3bd..f7fde4a onto 9fdb3bd
#
# Commands:
# p, pick = use commit
# r, reword = use commit, but edit the commit message
# e, edit = use commit, but stop for amending
# s, squash = use commit, but meld into previous commit
# f, fixup = like "squash", but discard this commit's log message
# x, exec = run command (the rest of the line) using shell
#
# These lines can be re-ordered; they are executed from top to bottom.
#
# If you remove a line here THAT COMMIT WILL BE LOST.
#
# However, if you remove everything, the rebase will be aborted.
#
# Note that empty commits are commented out
```

## [Change the most recent commit message](https://git-scm.com/docs/git-commit#Documentation/git-commit.txt---amend)

Commit that has not been pushed on remote

```
git commit --amend
```

If the commit was pushed the use
```
git push --force <branch>
```

## [Apply changes introduced by existing commit](https://git-scm.com/docs/git-cherry-pick)

```
git cherry-pick <commit>
```

Applies the changes introduced by commit `d02c73caff8c329f207298e5b20c568f5035a775` on current branch

```
git cherry-pick d02c73caff8c329f207298e5b20c568f5035a775
```

## [Revert changes](https://git-scm.com/docs/git-revert)

Revert some existing commits with a new commit

```
git revert <commit>
```

---
#### [< What is Git?](what-is-git.md)   -   [Best practices >](best-practices.md)
---
