#### [< Branches](branches.md)   -   [Merge Requests >](merge-requests.md)
---

# Branches

![Git flow model](images/git-flow-model.png)

## Main branches

* master
* develop(integration)

Depending on the way of working we could have or not a develop branch. Develop branch doesn’t make sense in case of using continuous deployment

`master` branch should always be the last stable and working version of code which is deployed to production.

## Supporting branches

* Feature branches
* Release branches
* Hotfix branches

### Feature branches

For each feature we create a specific feature branch, created from master branch. This branches are temporary while the ticket is implemented, reviewed, tested and merged. After the ticket is considered done and is merged the branch must be deleted.

Naming:

```
<PROJECT_ID>-<TICKET_ID>.<user>.short-description
```

### Release branch

Created for a specific release from master branch. This branch is temporary while the release is prepared tested and merged to master. Branch is release after it is merged to master and deployed to production.

Naming:

```
release-1.2.3
```

### Hotfix branch

Created from master for hotfixies. This branch includes in general only one critical urgent fix. It is a temporary branch while implementing, reviewing, testing and merging the fix. It should be deleted after merge and deploy to production.

Naming: 
```
hotfix-1.2.3a
```

# Tags
Tags are used for marking important points like releases

Create a tag
```
git tag -a <name>
```

List tags
```
git tag
```

---
#### [< Branches](branches.md)   -   [Merge Requests >](merge-requests.md)
---

