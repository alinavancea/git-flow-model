
#### [< Contents](README.md)   -   [Basic Git commands >](basic-git-commands.md)
---

# What is Git?

* Open source distributed version-control system for tracking changes in source code during software development, documenting processes and flows, digital projects.
* Developed in 2005 by Linus Torvalds described as “the stupid content tracker”
* Naming: random 3 letter-combination, “global information tracker” when in good mood and “goddamn idiotic truckload of sh*t” when breaks

---
#### [< Contents](README.md)   -   [Basic Git commands >](basic-git-commands.md)
---
