
#### [< Braches](braches.md)   -   [Testing, merging, cleaning >](test-merge-clean.md)
---

# Merge Requests(MR)

* A ticket can have multiple commits in a feature branch
* Rebase feature branch with master before opening a MR
* Open MR and prepare for review
* Make sure the tests are passing before sending the ticket to review
* From this moment it will be a discussion on the review and multiple commits could appear.


---
#### [< Braches](braches.md)   -   [Testing, merging, cleaning >](test-merge-clean.md)
---
