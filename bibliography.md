
#### [< Documentation commits](documentation-commits.md)   -   [Contents >](README.md)
---

# Bibliography

Bibliography and sources to study and improve your Git skills 

* [The official Git Book](https://git-scm.com/book/en/v2) 
* [Learn Git in 3 hours](https://learning.oreilly.com/library/view/learn-git-in/9781789348231/)
* [Git Flow Model](https://nvie.com/posts/a-successful-git-branching-model/) by Vincent Driessen
* [GitLab](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg/videos) and [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/videos)
* [Git Wikipedia](https://en.wikipedia.org/wiki/Git)
* https://github.com/erlang/otp/wiki/Writing-good-commit-messages
* https://wiki.openstack.org/wiki/GitCommitMessages
* http://chris.beams.io/posts/git-commit/
* https://ohmyz.sh/
* https://git-scm.com/docs/git-cherry-pick

---
#### [< Documentation commits](documentation-commits.md)   -   [Contents >](README.md)
---
