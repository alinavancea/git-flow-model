
#### [< Merge requests](merge-requests.md)   -   [Documentation commits >](documentation-commits.md)
---

# Test, merge and clean phase

Ticket is ready for testing, deployed in a testing env.

Consider rebase with master if ticket is old

```
git checkout your_branch
git pull origin master
git rebase -i master
```

After it is approved it is merged with master, consider a squash of the multiple commits if make sense.
Managing last 2 commits on the current branch

```
git checkout your_branch
git rebase -i HEAD~2 
```


https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html

Remove branch using GitLab UI or cmd

```
git branch -d branch_name
git push origin branch_name
```

---
#### [< Merge requests](merge-requests.md)   -   [Documentation commits >](documentation-commits.md)
---
