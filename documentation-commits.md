
#### [< Testing, merging, cleaning](test-merge-clean.md)   -   [Bibliography >](bibliography.md)
---


# Documentation commits

If we just want to push some changes to the documentation we don’t need to follow all this
workflow. When we only modify markdown files – so we are completely sure that the code is
unchanged – we can push our changes directly to master. In this case, as we don’t have a ticket
number we can add to the commit message, the commit format could be:

If there is a ticket assigned for the documentation change add the metadata of that ticket.

Prefer to avoid adding metadata of the ticket if there is an automatic pipeline that executes all the automatic tests. 
When changing the docs it is not necessary to run tests.

```
git commit -m "Doc: Summary of changes in docs"
```

---
#### [< Testing, merging, cleaning](test-merge-clean.md)   -   [Bibliography >](bibliography.md)
---
