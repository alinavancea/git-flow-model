# Git Flow Model

In this document you will find a proposal for a Git Flow Model which is general accepted by developers

## Contents

* [What is GIT](what-is-git.md)
* [Basic Git commands](basic-git-commands.md)
* [Best practices](best-practices.md)
* [Commits](commits.md)
* [Branches](branches.md)
* [Merge Request/Pull requests](merge-requests.md)
* [Testing, merging, cleaning](test-merge-clean.md)
* [Documentation commits](documentation-commits.md)
* [What we could use GitLab for?](what-we-could-use-gitlab-for.md)
* [Bibliography](bibliography.md)

![Git log](/images/git-logo.png)